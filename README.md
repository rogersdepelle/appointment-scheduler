### Appointment Scheduler API

* Requirements: [Docker](https://docs.docker.com/engine/install/ubuntu/#install-using-the-convenience-script) and [Docker Compose](https://docs.docker.com/compose/install/)
* Run locale: ``` docker-compose up ```
* Local server: [127.0.0.1:5000](http://127.0.0.1:5000/)
* Run tests locale: ``` docker-compose exec app python -m unittest --buffer ```
* API Documentation: https://app.swaggerhub.com/apis-docs/rogerspelle/appointment-scheduler/1.0.0

#### Endpoints:
*/appointment/schedule*
```bash
curl -X 'POST' \
  'http://127.0.0.1:5000/appointment/schedule' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
  "user_email": "user@email.com",
  "start_datetime": "2020-01-01 01:00"
}'
```

*/appointment/listByUser/{user_email}*
```bash
curl -X 'GET' \
  'http://127.0.0.1:5000/appointment/listByUser/user@email.com' \
  -H 'accept: application/json'
```
