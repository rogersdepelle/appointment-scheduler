import datetime
import re


def form_validation(data):
    errors = {}
    user_email = None
    start_datetime = None

    if data is None:
        return {"Payload": "Empty Payload"}, user_email, start_datetime

    if "user_email" not in data:
        errors["user_email"] = "'user_email' is a required property"
    else:
        user_email = data["user_email"]
        if not re.fullmatch(r"\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b", user_email):
            errors["user_email"] = "'user_email' must be a valid email address"

    if "start_datetime" not in data:
        errors["start_datetime"] = "'start_datetime' is a required property"
    else:
        start_datetime = data["start_datetime"]
        try:
            start_datetime = datetime.datetime.strptime(start_datetime, "%Y-%m-%d %H:%M")
            if start_datetime.minute not in [0, 30]:
                errors["start_datetime"] = "All appointments must to start on the hour or half-hour"
        except ValueError:
            errors["start_datetime"] = "'start_datetime' must be a valid datetime (YYYY-MM-DD HH:MM)"

    return errors, user_email, start_datetime
