import datetime
import pickle
import redis

from flask import Flask, request, jsonify
from flask.json import JSONEncoder

from utils import form_validation


class CustomJSONEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime.datetime):
            return obj.strftime("%Y-%m-%d %H:%M")
        return JSONEncoder.default(self, obj)


app = Flask(__name__)
app.json_encoder = CustomJSONEncoder
cache = redis.Redis(host="redis", port=6379)


def get_appointments(user_email):
    appointments = cache.get(user_email)
    if appointments is None:
        appointments = []
    else:
        appointments = pickle.loads(appointments)
    return appointments


@app.route("/appointment/schedule", methods=["POST"])
def appointment_schedule():
    errors, user_email, start_datetime = form_validation(request.json)
    if errors:
        return jsonify({"errors": errors}), 400

    appointments = get_appointments(user_email)
    if appointments:
        appointments_dates = [a["start_datetime"].date() for a in appointments]
        if start_datetime.date() in appointments_dates:
            errors["start_datetime"] = "A user can only have 1 appointment on a calendar date"
            return jsonify({"errors": errors}), 400

    appointment = {"start_datetime": start_datetime, "end_datetime": start_datetime + datetime.timedelta(minutes=30)}
    appointments.append(appointment)
    cache.set(user_email, pickle.dumps(appointments))

    return jsonify(appointment), 201


@app.route("/appointment/listByUser/<string:user_email>", methods=["GET"])
def appointment_list_by_user(user_email):
    appointments = get_appointments(user_email)
    return jsonify(appointments)
