import datetime
import json
import pickle
import unittest

from app import app, cache


class APITestCase(unittest.TestCase):
    user_email = "test_user@email.com"

    def setUp(self):
        self.app = app.test_client()
        appointments = [
            {
                "start_datetime": datetime.datetime.strptime("2000-01-01 01:00", "%Y-%m-%d %H:%M"),
                "end_datetime": datetime.datetime.strptime("2000-01-01 01:30", "%Y-%m-%d %H:%M"),
            },
            {
                "start_datetime": datetime.datetime.strptime("2000-01-02 01:00", "%Y-%m-%d %H:%M"),
                "end_datetime": datetime.datetime.strptime("2000-01-02 01:30", "%Y-%m-%d %H:%M"),
            },
        ]
        cache.set(self.user_email, pickle.dumps(appointments))

    def tearDown(self):
        cache.delete(self.user_email)

    def test_appointment_list(self):
        response = self.app.get(f"/appointment/listByUser/{self.user_email}")
        self.assertEqual(200, response.status_code)
        expected_response = [
            {"end_datetime": "2000-01-01 01:30", "start_datetime": "2000-01-01 01:00"},
            {"end_datetime": "2000-01-02 01:30", "start_datetime": "2000-01-02 01:00"},
        ]
        self.assertEqual(expected_response, response.json)

    def test_appointment_empty_list(self):
        response = self.app.get("/appointment/listByUser/another_user@email.com")
        self.assertEqual(200, response.status_code)
        self.assertEqual([], response.json)

    def test_appointment_schedule(self):
        data = {"user_email": self.user_email, "start_datetime": "2000-01-03 01:00"}
        response = self.app.post(
            "/appointment/schedule", data=json.dumps(data), headers={"Content-Type": "application/json"}
        )
        self.assertEqual(201, response.status_code)
        expected_response = {"start_datetime": "2000-01-03 01:00", "end_datetime": "2000-01-03 01:30"}
        self.assertEqual(expected_response, response.json)

    def test_appointment_schedule_form_validation(self):
        response = self.app.post(
            "/appointment/schedule", data=json.dumps({}), headers={"Content-Type": "application/json"}
        )
        self.assertEqual(400, response.status_code)
        expected_response = {
            "user_email": "'user_email' is a required property",
            "start_datetime": "'start_datetime' is a required property",
        }
        self.assertEqual(expected_response, response.json["errors"])

        data = {"user_email": "username", "start_datetime": "2000-01-01"}
        response = self.app.post(
            "/appointment/schedule", data=json.dumps(data), headers={"Content-Type": "application/json"}
        )
        self.assertEqual(400, response.status_code)
        expected_response = {
            "user_email": "'user_email' must be a valid email address",
            "start_datetime": "'start_datetime' must be a valid datetime (YYYY-MM-DD HH:MM)",
        }
        self.assertEqual(expected_response, response.json["errors"])

        data = {"user_email": self.user_email, "start_datetime": "2000-01-01 01:01"}
        response = self.app.post(
            "/appointment/schedule", data=json.dumps(data), headers={"Content-Type": "application/json"}
        )
        self.assertEqual(400, response.status_code)
        expected_response = {
            "start_datetime": "All appointments must to start on the hour or half-hour",
        }
        self.assertEqual(expected_response, response.json["errors"])

        data = {"user_email": self.user_email, "start_datetime": "2000-01-01 01:00"}
        response = self.app.post(
            "/appointment/schedule", data=json.dumps(data), headers={"Content-Type": "application/json"}
        )
        self.assertEqual(400, response.status_code)
        expected_response = {
            "start_datetime": "A user can only have 1 appointment on a calendar date",
        }
        self.assertEqual(expected_response, response.json["errors"])
